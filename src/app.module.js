import angular from 'angular';
import CustomButtonComponent from './custom-button/custom-button.component';

angular.module('MainApp', [])
	.component('customButton', CustomButtonComponent);