import controller from './custom-button.controller';
import template from './custom-button.template.html';
import './custom-button.scss';

let CustomButtonComponent = {
    template,
    controller,
    controllerAs: 'vm',
    replace: true,
    bindings: {
        label: '@'
    }
};

export default CustomButtonComponent;
