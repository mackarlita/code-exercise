var path = require('path');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://127.0.0.1:1313',
		'./src/app.module.js'
	],
	output: {
		filename: 'webpack.bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	resolveLoader: {
		modules: ['node_modules']
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.html$/,
				exclude: /node_modules/,
				loader: 'html-loader'
			},
			{
				test: /\.scss$/,
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader' },
					{ loader: 'sass-loader' }
				]
			}
		]
	},	
	devServer: {
	  contentBase: './src/',
	  port: 1313
	}	
}